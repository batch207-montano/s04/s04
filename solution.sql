A. Find all artists that has letter d in its name.

    SELECT * FROM artists WHERE name LIKE("%d%");

B. Find all albums that starts with b in its name.

    SELECT * FROM albums WHERE album_title LIKE("b%");

C. Join the 'artists' and 'albums' tables. (Find all artists that has letter a at the start of its name.)

    SELECT * FROM artists 
    JOIN albums ON albums.id = albums.artist_id 
    WHERE name LIKE("a%");

D. Sort the albums in Z-A order. (Show only the first 4 records.)

    SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

E. Sort the artists in A-Z order. (Show only the first 5 records.)
    
    SELECT * FROM artists ORDER BY name ASC LIMIT 5;