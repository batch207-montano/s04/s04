=========================================
SQL - Advanced Selects and Joining Tables
=========================================

INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Ariana Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Taylor Swift

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Fearless", "2008", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Fearless", 246, "Pop rock", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Love Story", 213, "Country pop", 3);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Red", "2012", 3);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("State of Grace", 273, "Rock, alternative rock, arena rock", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Red", 204, "Country", 4);

-- Lady Gaga

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("A Star Is Born", "2018", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Black Eyes", 181, "Rock and roll", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Shallow", 201, "Country, rock, folk rock", 5);	

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Born This Way", "2011", 4);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Born This Way", 252, "Electropop", 6);

-- Justin Bieber

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Purpose", "2015", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Sorry", 192, "Dancehall-poptropical housemoombahton", 7);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Believe", "2012", 5);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Boyfriend", 251, "Pop", 8);

-- Ariana Grande

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Dangerous Woman", "2016", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Into You", 242, "EDM house", 9);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Thank U, Next", "2019", 6);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Thank U Next", 196, "Pop, R&B", 10);

-- Bruno Mars

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("24K Magic", "2016", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("24K Magic", 207, "Funk, disco, R&B", 16);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Earth to Mars", "2011", 7);
INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Lost", 192, "Pop", 17);


1. Exclude records.
	Terminal

		Excluding records:
		Syntax
			SELECT column_name FROM table_name WHERE column_name != value;

		SELECT * FROM songs WHERE id != 11;
		SELECT * FROM songs WHERE album_id != 5 AND album_id != 6;


2. Finding records using comparison operators.
	Terminal

		Displaying/retrieving records with comparison operators:

		SELECT * FROM songs WHERE length > 230;
		SELECT * FROM songs WHERE length < 200;
		SELECT * FROM songs WHERE length > 230 OR length < 200;

3. Getting records with specific conditions.
	Terminal

		IN CLAUSE
		Can be used for querying multiple columns:
		SELECT * FROM songs WHERE id = 1 OR id = 3 OR id = 5;

		Best used for querying multiple values in a single column:
		SELECT * FROM songs WHERE id IN (1, 3, 5);
		SELECT * FROM songs WHERE genre IN ("Pop", "K-pop");

4. Show records with a partial match.
    Terminal

        Displaying/retrieving records with a partial match:
        LIKE CLAUSE

        Find values with a match at the start
        SELECT * FROM songs WHERE song_name LIKE "th%";

        Find values with a match at the end
        SELECT * FROM songs WHERE song_name LIKE "%ce";

        Find values with a match at any position
        SELECT * FROM artists WHERE name LIKE "%run%";

        Find values with a match of a specific length/pattern
        SELECT * FROM songs WHERE song_name LIKE "__rr_";

        Find values with a match at certain positions
        SELECT * FROM albums WHERE album_title LIKE "_ur%";

        IMPORTANT NOTE:
        	- The percent (%) and underscore (_) symbols are called wildcard operators 
            - The percent (%) symbol represents zero or multiple characters
            - The underscore (_) represents a single character

5. Sorting records.
    Terminal

        Sorting records:
        Syntax
        	SELECT column_name FROM table_name ORDER BY column_name ORDER;
        
        SELECT * FROM songs ORDER BY song_name;
        SELECT * FROM songs ORDER BY song_name ASC;
        SELECT * FROM songs ORDER BY length DESC;

        IMPORTANT NOTE:
        	- The value of the ORDER keyword can be "ASC" for ascending and  "DESC" for descending

6. Limiting Records.
	Terminal

		Limiting records:
		SELECT * FROM songs LIMIT 5;

7. Showing records with distinct values.
    Terminal

        Displaying/retrieving records with distinct values:
        SELECT genre FROM songs;
        SELECT DISTINCT genre FROM songs;

8. Joining two tables.
	Terminal

		Joining two tables:
		Syntax
			SELECT column_name FROM table1
				JOIN table2 ON table1.id = table2.foreign_key_column;

		SELECT * FROM artists 
    JOIN albums ON artists.id = albums.artist_id;

9. Joining multiple tables.
	Terminal

		Joining multiple tables:
		Syntax
			SELECT column_name FROM table1
				JOIN table2 ON table1.id = table2.foreign_key_column
				JOIN table3 ON table2.id = table3.foreign_key_column; 

		SELECT * FROM artists 
		    JOIN albums ON artists.id = albums.artist_id 
		    JOIN songs ON albums.id = songs.album_id;

10. Joining tables with specified WHERE conditions
	
		SELECT * FROM artists 
			JOIN albums ON artists.id = albums.artist_id 
			WHERE name LIKE "%a%";

10. Selecting colums to be displayed from joining multiple tables.
	Terminal

		SELECT name, album_title, date_released, song_name, length, genre FROM artists 
		    JOIN albums ON artists.id = albums.artist_id 
		    JOIN songs ON albums.id = songs.album_id;

11. Providing aliases for joining tables.
	Terminal

		Syntax
			SELECT column_name AS alias FROM table1
				JOIN table2 ON table1.id = table2.foreign_key_column
				JOIN table3 ON table2.id = table3.foreign_key_column;

		SELECT name AS band, album_title AS album, date_released, song_name AS song, length, genre FROM artists 
		    JOIN albums ON artists.id = albums.artist_id 
		    JOIN songs ON albums.id = songs.album_id;

12. Displaying data from joining tables.
	Terminal

		/*Create user information, a playlist and songs added to the user playlist:*/
		INSERT INTO users (username, password, full_name, contact_number, email, address) VALUES ("john", "john1234", "John Doe", 09123456789, "john@mail.com", "New York");

		INSERT INTO playlists (user_id, datetime_created) VALUES (1, "2021-01-02 01:00:00");

		INSERT INTO playlists_songs (playlist_id, song_id) VALUES (1, 1), (1, 10), (1, 11);

		Joining multiple tables:
		SELECT * FROM playlists
			JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
			JOIN songs ON playlists_songs.song_id = songs.id;

		Selecting specific columns to be displayed from the query:
		SELECT user_id, datetime_created, song_name, length, genre, album_id FROM playlists
			JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
			JOIN songs ON playlists_songs.song_id = songs.id;
